# chord

## Description

Implementation of the Chord algorithm in Clojure. Work-in-progress.

## References

1. Ion Stoica, Robbert Marris, David Karger, Frans M. Kaashoek, Hari
   Balakrishnan. *Chord: A scalable peer-to-peer lookup service for internet
   applications*. SIGCOMM computer communication
   review. 2011. https://pdos.csail.mit.edu/papers/chord:sigcomm01/chord_sigcomm.pdf
