(def project 'kv)
(def version "0.1.0-SNAPSHOT")

(set-env! :resource-paths #{"src"}
          :source-paths   #{"test"}
          :dependencies   '[[org.clojure/clojure "1.9.0"]
                            [org.clojure/algo.generic "0.1.2"]
                            [adzerk/boot-test "1.2.0" :scope "test"]
                            [buddy/buddy-core "1.4.0"]
                            [mount "0.1.12"]
                            [org.clojure/core.async "0.4.474"]
                            [slacker "0.17.0"]
                            [aero "1.1.3"]
                            [com.fzakaria/slf4j-timbre "0.3.8"]
                            [org.slf4j/log4j-over-slf4j "1.7.25"]
                            [org.slf4j/jul-to-slf4j "1.7.25"]
                            [org.slf4j/jcl-over-slf4j "1.7.25"]
                            [org.slf4j/log4j-over-slf4j "1.7.25"]
                            [com.taoensso/timbre "4.10.0"]
                            [com.stuartsierra/component "0.3.2"]])


(task-options!
 aot {:namespace   #{'kv.core}}
 pom {:project     project
      :version     version
      :description "FIXME: write description"
      :url         "http://example/FIXME"
      :scm         {:url "https://github.com/yourname/clj-kv"}
      :license     {"Eclipse Public License"
                    "http://www.eclipse.org/legal/epl-v10.html"}}
 jar {:main        'kv.core
      :file        (str "kv-" version "-standalone.jar")})

(deftask build
  "Build the project locally as a JAR."
  [d dir PATH #{str} "the set of directories to write to (target)."]
  (let [dir (if (seq dir) dir #{"target"})]
    (comp (aot) (pom) (uber) (jar) (target :dir dir))))

(deftask run
  "Run the project."
  [a args ARG [str] "the arguments for the application."]
  (require '[kv.core :as app])
  (apply (resolve 'app/-main) args))

(require '[adzerk.boot-test :refer [test]])
