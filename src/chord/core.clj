(ns chord.core
  (:require [clojure.spec.alpha :as s]
            [buddy.core.hash :as hash]
            [slacker.server :refer [start-slacker-server stop-slacker-server]]
            [slacker.client :as client]
            [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [clojure.algo.generic.math-functions :as math]))

(s/def ::port pos?)
(s/def ::host string?)
(s/def ::adress (s/keys :req-un [::host ::port]))
(s/def ::chord-id #(or (pos? %) (= 0 %)))
(s/def ::node-desc (s/keys :req-un [::chord-id ::adress]))
(s/def ::finger-table (s/coll-of ::node-desc :kind vector?))
(s/def ::modulus pos?)

;; TODO: make these work with `query-predecessor` and other, by changing the
;; return value.
(s/def ::status #{:done :redirect})
(s/def ::redirection (s/keys :req-un [::status]
                             :opt-un [::node-desc]))

(defn- pow
  "Power function for integral arguments. Returns an integer (as apposed to a
  floating point number)."
  [x y]
  (int (math/pow x y)))

(def ^:private sha1-modulus (biginteger (math/pow 2 160)))

(defn adress->chord-id
  ([adress] (adress->chord-id adress sha1-modulus))
  ([{host :host port :port :as adress} modulus]
   {:pre [(s/valid? ::adress adress)
          (s/valid? ::modulus modulus)]
    :post [(s/valid? ::chord-id %)]}
   (mod (biginteger (hash/sha1 (str host ":" port))) modulus)))

(defn between?
  "Checks whether `x` lies between `start` and `end` on a cycle (of unknown
  length)."
  ([start end {start-bound :start end-bound :end} x]
   (let [start-op (if (= start-bound :exclusive) < <=)
         end-op (if (= end-bound :exclusive) < <=)]
     (cond
       (<= start end) (and (start-op start x) (end-op x end))
       (> start end) (or (start-op start x) (end-op x end)))))
  ([start end x] (between? start end {} x)))

(defn predecessor?
  "Checks whether `id` maps to the successor of the node in the second argument,
  i.e., the predecessor of an identifier is the largest node that preceeds it."
  [id {:keys [self-desc finger-table-ref]}]
  (let [start (:chord-id self-desc)
        end (-> @finger-table-ref first :chord-id)]
    (or (= start end)
        (between? start end {:start :exclusive} id))))

(defn preceeds?
  "Checks whether `finger-id` preceeds `id` when seen from `node-id`."
  [node-id id finger-id]
  ;; A finger preceeds an identifier from the standpoint of a node, if jumping
  ;; there gets you closer the predecessor of the identifier. Everything should
  ;; preceed the node's own id, except the node itself. The latter condition is
  ;; necessary to avoid getting stuck on a single node when looking for the
  ;; predecessor of an identifier.
  (or (and (= node-id id) (not= id finger-id))
      (between? node-id id {:start :exclusive :end :exclusive} finger-id)))

(defn closest-preceding-finger
  [{:keys [self-desc finger-table-ref]} id]
  (let [node-id (:chord-id self-desc)]
    (->> (reverse @finger-table-ref)
         (filter (fn [f] (preceeds? node-id id (:chord-id f))))
         (first))))

(defn query-predecessor*
  "Returns information the node has about the predecessor of `id`. The node
  responds by either stating it is the predecessor, or by redirecting to a
  closer node that preceeds `id`."
  [node id]
  (if (predecessor? id node)
    {:status :done
     :node node}
    {:status :redirect
     :node (closest-preceding-finger node id)}))

(defn query-predecessor
  "Queries the node about the successor of `id`. Simple wrapper around the
  function `query-predecessor*`."
  [node id]
  (let [r (query-predecessor* node id)]
    (case (:status r)
      :done {:status :done :node (:self-desc node)}
      :redirect r)))

(defn successor-desc
  "Return the successor of the node, i.e. the first element of its finger table."
  [node]
  {:post [(s/valid? ::node-desc %)]}
  (first @(:finger-table-ref node)))

(defn query-successor
  "Queries the node about the successor of `id`. Simple wrapper around the
  function `query-predecessor*`."
  [node id]
  (let [r (query-predecessor* node id)]
    (case (:status r)
      :done {:status :done :node (successor-desc node)}
      :redirect r)))

(defn insert
  "Inserts the node specified by `node-desc` as the predecessor of `node`. Returns
  the previous predecessor. It is the responsibility of the client to check
  whether it is inserted at the right position."
  [{pred-desc-ref :pred-desc-ref} node-desc]
  {:pre [(s/valid? ::node-desc node-desc)]
   :post [(s/valid? ::node-desc %)]}
  (let [previous @pred-desc-ref]
    (reset! pred-desc-ref node-desc)
    previous))

(defn- clockwise-distance
  "Distance from `from` to `to` on the circle in clockwise direction."
  [modulus from to]
  (mod (- to from) modulus))

(defn finger-start
  "Compute the id (or index) of which the finger at `finger-index` is the
  successor, for the node with id `node-id`. The modulus is the maximum
  identifier plus one.

  Note that the fingers are indexed starting from 0, this is different from the
  paper, where indices start at 1."
  [node-id finger-index modulus]
  (mod (+ node-id (pow 2 finger-index)) modulus))

(defn update-finger?
  "Checks whether finger with identifier `finger-id` should be set as the new
  finger at index `index` of node `node`."
  [{:keys [modulus finger-table-ref self-desc]} index new-finger-id]
  (let [old-finger-id (:chord-id (nth @finger-table-ref index))
        node-id (:chord-id self-desc)
        start (finger-start node-id index modulus)
        dist (partial clockwise-distance modulus)]
    ;; This is different from the article. Consider the case of joining the node
    ;; with id 0 to a chord consisting of a node with id 1. Following the steps
    ;; of the algorithm in Figure 6 gives:
    ;;
    ;; 1. All fingers of node 0 are set to 1. (init-finger-table)
    ;;
    ;; 2. Whether to update a fingers of node 0 to 0 depends on the index, since
    ;;    the actual fingers of node 0 should be [1 0 0]. However, the decision
    ;;    on whether to update the finger only depends on the node id, the id of
    ;;    the old finger, and the id of the potentially new finger. I am either
    ;;    missing something, or there is an error in the
    ;;    article. (update-finger-table)
    (< (dist start new-finger-id) (dist start old-finger-id))))


(defn update-finger-table
  "Determine whether the node with descriptor `node-desc` qualifies as the finger
  at index `index` for the node in the first argument, and if so, update the
  finger table."
  [{:keys [pred-desc-ref self-desc finger-table-ref] :as node} node-desc index]
  (log/debugf "node = %s, index = %s, node-desc = %s" (:chord-id self-desc) index node-desc)
  (if (update-finger? node index (:chord-id node-desc))
    (do
      (swap! finger-table-ref #(assoc % index node-desc))
      {:status :redirect
       :node @pred-desc-ref})
    {:status :done}))


(defn api
  [node]
  {"chord" {"query-predecessor" (partial query-predecessor node)
            "query-successor" (partial query-successor node)
            "insert" (partial insert node)
            "update-finger-table" (partial update-finger-table node)}})

(defn start-rpc-server
  [{:keys [self-desc] :as node}]
  (start-slacker-server (api node) (-> self-desc :adress :port)))

(defrecord Node [self-desc pred-desc-ref finger-table-ref modulus rpc-server]
  component/Lifecycle

  (start [this]
    (if-not rpc-server
      (let [{port :port host :host} (:adress self-desc)]
        (log/infof "Starting RPC server on %s:%s" host port)
        (assoc this :rpc-server (start-rpc-server this)))
      this))

  (stop [this]
    (when rpc-server
      (let [{port :port host :host} (:adress self-desc)]
        (log/infof "Stopping RPC server on %s:%s" host port)
        (stop-slacker-server rpc-server)
        (assoc this :rpc-server nil)))))

(defn finger-count
  [modulus]
  (loop [q modulus i 0]
    (if (>= q 2) (recur (quot q 2) (inc i)) i)))

(defn new-node
  "Construct a new node. The node's RPC server runs on `adress`, its identifier
  is `chord-id` and the identifiers of the chord that the node will be running
  in lie on a circle of length `modulus`.

  In the 1- and 2-arity variants the node's identifier is constructed by hashing
  its adress. The implicit modulus in the 1-arity variant is 2^160, where
  160 is the number of bits in the output of the SHA-1 hash function."
  ([adress]
   (new-node adress sha1-modulus))
  ([adress modulus]
   (new-node adress (adress->chord-id adress modulus) modulus))
  ([adress chord-id modulus]
   {:pre [(s/valid? ::adress adress)
          (s/valid? ::chord-id chord-id)
          (s/valid? ::modulus modulus)]}
   (let [self-desc {:adress adress
                    :chord-id chord-id}
         finger-table (repeat (finger-count modulus) self-desc)]
     (map->Node {:self-desc self-desc
                 :finger-table-ref (atom (into [] finger-table))
                 :pred-desc-ref (atom self-desc)
                 :modulus modulus}))))



;; client code

(defn call-node
  [{host :host port :port} fn-name & args]
  (let [sc (client/slackerc (str host ":" port))]
    (client/call-remote sc "chord" fn-name args)))

(defn until-done
  [f adress]
  (loop [next-adress adress]
    (let [response (f next-adress)]
      (if (= (:status response) :done)
        response
        (recur (-> response :node :adress))))))

(defn call-insert
  [node-desc adress]
  {:pre [(s/valid? ::node-desc node-desc)
         (s/valid? ::adress adress)]
   :post [(s/valid? ::node-desc %)]}
  (call-node adress "insert" node-desc))

(defn call-query-predecessor
  [id adress]
  {:pre [(s/valid? ::adress adress)
         (s/valid? ::chord-id id)]}
  (call-node adress "query-predecessor" id))

(defn call-query-successor
  [id adress]
  {:pre [(s/valid? ::adress adress)
         (s/valid? ::chord-id id)]}
  (call-node adress "query-successor" id))

(defn find-predecessor
  "Finds the predecessor of `id`, by repeated querying, starting with an initial
  node on adress `adress`."
  [id adress]
  {:pre [(s/valid? ::adress adress)
         (s/valid? ::chord-id id)]
   :post [(s/valid? ::node-desc %)]}
  (:node (until-done (partial call-query-predecessor id) adress)))

(defn find-successor
  "Finds the successor of `id`, by repeated querying, starting with an initial
  node on adress `adress`."
  [id adress]
  {:pre [(s/valid? ::adress adress)
         (s/valid? ::chord-id id)]
   :post [(s/valid? ::node-desc %)]}
  (:node (until-done (partial call-query-successor id) adress)))

(defn potential-finger-for
  "Compute the id such that nodes before this id may have `node-id` as the finger
  at index `finger-index`."
  [node-id finger-index modulus]
  ;; I think there is a mistake in the paper.
  (mod (inc (- node-id (pow 2 finger-index))) modulus))

(defn node-id
  [{self-desc :self-desc}]
  (:chord-id self-desc))

(defn find-finger
  [{modulus :modulus :as node} adress index]
  (let [start (finger-start (node-id node) index modulus)]
    (find-successor start adress)))

(defn insert-and-init-predecessor!
  "Inserts the node into the chords and sets the predecessor of the node. Requires
  the finger table to be already initialized."
  [node]
  (->> (:adress (first @(:finger-table-ref node)))
       (call-insert (:self-desc node))
       (reset! (:pred-desc-ref node) )))

(defn init-finger-table!
  [{modulus :modulus :as node} seed]
  {:pre [(s/valid? ::modulus modulus)
         (s/valid? ::adress seed)]}
  ;; TODO: Unoptimized, requires (log n)^2 messages
  (->> (range (finger-count modulus))
       (pmap (partial find-finger node seed))
       (into [])
       (reset! (:finger-table-ref node))))

(defn call-update-finger-table
  [node-desc index adress]
  {:pre [(s/valid? ::node-desc node-desc)
         (s/valid? int? index)
         (s/valid? ::adress adress)]}
  (call-node adress "update-finger-table" node-desc index))

(defn update-finger-tables-until-done
  "Updates the finger-table of the node at adress `adress` and its predecessors,
  when necessary. The update consists of setting the finger at index `index` to
  `node-desc`."
  [node-desc index adress]
  {:pre [(s/valid? ::node-desc node-desc)
         (s/valid? int? index)
         (s/valid? ::adress adress)]}
  (until-done (partial call-update-finger-table node-desc index) adress))

(defn update-others
  [{:keys [self-desc modulus]} seed]
  (doall
    (pmap (fn [index]
            (-> (potential-finger-for (:chord-id self-desc) index modulus)
                (find-predecessor seed)
                (#(update-finger-tables-until-done self-desc index (:adress %)))))
          (range (finger-count modulus))))
  nil)

(defn join!
  "Join a chord by contacting a seed node."
  ([{:keys [self-desc finger-table-ref pred-desc-ref modulus]}]
   (reset! finger-table-ref (repeat (finger-count modulus) self-desc))
   (reset! pred-desc-ref self-desc))
  ([node seed]
   (init-finger-table! node seed)
   (insert-and-init-predecessor! node)
   (update-others node seed)
   node))
