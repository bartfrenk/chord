(ns chord.core-test
  (:require [chord.core :as sut]
            [clojure.test :refer [deftest testing is are] :as t]
            [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]))

(defn pmapv
  [f m]
  (into {} (pmap (fn [[k v]] [k (f v)]) m)))

(defn- get-free-port
  "Returns the port number of an available port."
  []
  (let [socket (java.net.ServerSocket. 0)]
    (.close socket)
    (.getLocalPort socket)))

(defn- turn-off-logging
  [f]
  (log/set-level! :warn)
  (f))

(t/use-fixtures :once turn-off-logging)

(defn- set-predecessor!
  [node pred]
  (reset! (:pred-desc-ref node) (:self-desc pred)))

(defn- set-finger-table!
  [node finger-table-nodes]
  (reset! (:finger-table-ref node) (into [] (map :self-desc finger-table-nodes))))

(defn- chord-example-1
  "Create the chord example of Figure 2. It has eight valid identifiers, and
  nodes on 0, 1 and 3."
  []
  (let [adresses (repeatedly 3 (fn [] {:host "localhost"
                                        :port (get-free-port)}))
        node-0 (sut/new-node (nth adresses 0) 0 8)
        node-1 (sut/new-node (nth adresses 1) 1 8)
        node-3 (sut/new-node (nth adresses 2) 3 8)]
    (set-predecessor! node-0 node-3)
    (set-predecessor! node-1 node-0)
    (set-predecessor! node-3 node-1)
    (set-finger-table! node-0 [node-1 node-3 node-0])
    (set-finger-table! node-1 [node-3 node-3 node-0])
    (set-finger-table! node-3 [node-0 node-0 node-0])
    {0 node-0 1 node-1 3 node-3}))

(defn- start-chord
  [chord]
  (pmapv component/start chord))

(defn- stop-chord
  [chord]
  (pmapv component/stop chord))

(deftest map-identifiers-to-nodes-in-chord-example-1-test
  (let [chord (start-chord (chord-example-1))]
    (try
      (testing "map identifier to predecessor"
        (doseq [[id predecessor-id] [[0 3]
                                     [1 0]
                                     [2 1]
                                     [3 1]
                                     [4 3]
                                     [5 3]
                                     [6 3]
                                     [7 3]]
                {node-desc :self-desc} (vals chord)]
          (let [actual (sut/find-predecessor id (:adress node-desc))]
            (is (= predecessor-id (:chord-id actual))
                (str "id = " id ", start = " node-desc)))))
      (testing "map identifier to successor"
        (doseq [[id successor-id] [[0 0]
                                   [1 1]
                                   [2 3]
                                   [3 3]
                                   [4 0]
                                   [5 0]
                                   [6 0]
                                   [7 0]]
                {node-desc :self-desc} (vals chord)]
          (let [actual (sut/find-successor id (:adress node-desc))]
            (is (= successor-id (:chord-id actual))
                (str "id = " id ", start = " node-desc)))))
      (finally (stop-chord chord)))))


(deftest query-predecessor-in-chord-example-1-test
  (let [chord (chord-example-1)]
    (testing "returns correct response for all IDs and nodes"
      (let [cases [{:node-id 0 :id 0 :expected {:status :redirect :node-id 3}}
                   {:node-id 0 :id 1 :expected {:status :done :node-id 0}}
                   {:node-id 0 :id 2 :expected {:status :redirect :node-id 1}}
                   {:node-id 0 :id 4 :expected {:status :redirect :node-id 3}}
                   {:node-id 1 :id 0 :expected {:status :redirect :node-id 3}}
                   {:node-id 1 :id 2 :expected {:status :done :node-id 1}}
                   {:node-id 3 :id 0 :expected {:status :done :node-id 3}}
                   {:node-id 3 :id 2 :expected {:status :redirect :node-id 0}}]]
        (doseq [{:keys [node-id id expected] :as case} cases]
          (let [actual (sut/query-predecessor (get chord node-id) id)]
            (is (= (:status expected) (:status actual)) (str case))
            (is (= (:node-id expected) (-> actual :node :chord-id)) (str case))))))))

(defn new-disjoint-chord
  [node-ids modulus]
  (->> (repeatedly (fn [] {:host "localhost"
                           :port (get-free-port)}))
       (map (fn [i a] [i (sut/new-node a i modulus)]) node-ids)
       (into {})))

(deftest joining-three-nodes-into-a-chord
  (letfn [(finger-table-ids [{finger-table-ref :finger-table-ref}]
            (map :chord-id @finger-table-ref))
          (predecessor-id [{pred-desc-ref :pred-desc-ref}]
            (:chord-id @pred-desc-ref))]
    (let [chord (start-chord (new-disjoint-chord [0 1 3] 8))]
      (try
        (sut/join! (get chord 0) (-> chord (get 1) :self-desc :adress))
        (sut/join! (get chord 3) (-> chord (get 0) :self-desc :adress))
        (testing "all predecessors are correct"
          (is (= (predecessor-id (get chord 0)) 3))
          (is (= (predecessor-id (get chord 1)) 0))
          (is (= (predecessor-id (get chord 3)) 1)))
        (testing "all finger tables are correct"
          (is (= (finger-table-ids (get chord 0)) [1 3 0]))
          (is (= (finger-table-ids (get chord 1)) [3 3 0]))
          (is (= (finger-table-ids (get chord 3)) [0 0 0])))
        (finally (stop-chord chord))))))


;; ==== Testing code (TODO: to remove) ====

;; (def chord (new-disjoint-chord [0 1 3] 8))

;; (defn start []
;;   (alter-var-root #'chord (partial pmapv component/start)))
;; (defn stop []
;;   (alter-var-root #'chord (partial pmapv component/stop)))

;; (start)
;; (stop)

;; (sut/find-finger (get chord 0) 8 (-> chord (get 1) :self-desc :adress) 0)

;; (sut/find-successor 4 (-> chord (get 1) :self-desc :adress))

;; (sut/init-finger-table! (get chord 0) 8 (-> chord (get 1) :self-desc :adress))
;; (sut/join! (get chord 3) 8  (-> chord (get 1) :self-desc :adress))

;; (def node (sut/new-node {:host "localhost" :port 4444} 5))
;; (defn seed [] (-> chord (get 0) :self-desc :adress))

;; (sut/update-finger-table (-> chord (get 3)) 1 (:self-desc node))
;; (sut/update-finger-table node 1 (:self-desc node))

;; (sut/join! node 8 (seed))
;; (sut/update-others node 8 (seed))

;; (sut/call-query-predecessor 0  (-> chord (get 0) :self-desc :adress))

;; (sut/find-successor 0 (-> chord (get 0) :self-desc :adress))

;; (sut/closest-preceding-finger (get chord-example-1 0) 0)

;; (get chord-example-1 0)

;; (-> chord-example-1 (get 0) :self-desc :adress)

;; (get chord-example-1 0)
;; (sut/find-predecessor (-> chord-example-1 (get 0) :self-desc :adress) 2)
;; (sut/predecessor? (get chord-example-1 0) 2)






